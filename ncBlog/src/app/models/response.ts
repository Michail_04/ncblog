import { Posts } from './state/posts';

export interface Response {

}

export interface LoginErrorResponse extends Response{
    message?: string,
    error: boolean
}
export interface LoginSuccesResponse extends Response{
  token?: string;
  id: string;
  name: string;
  ruls: number;
}

export interface LoginCredential {
  email: string;
  password: string;
}
export interface SigninCredential extends LoginCredential {
  name: string;
}

export interface checkTokenResp extends LoginSuccesResponse{
  
}

export interface SectionsResp {
  _id: string| number;
  title: string;
  dateAdd: Date;
  dateUpd: Date;
}

export interface PostsResponse {
  posts: Posts[];
  hasNext: boolean;
  hasPosts: boolean;
  hasPrev: boolean;
  page: number;
  totalPages: number;
  postsOnPage: number;
  totalPosts: number;
}
