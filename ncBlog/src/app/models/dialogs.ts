export enum DialogType{
     AuthDialog,
     SignUpDialog,
     ResetPassDialog,
     TokenDialog
}