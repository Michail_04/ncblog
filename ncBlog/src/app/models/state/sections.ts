export interface SectionsState{
    sections: SectionsBlock;
}

export interface SectionsBlock{
    data: Sections[];
    status: boolean;
    error: boolean;
    errorMsg?: string;
}

export interface Sections{
    id: string| number;
    title: string;
    dateAdd: Date;
    dateUpd: Date;
    checked?: boolean;
    posts?: any[]
}
