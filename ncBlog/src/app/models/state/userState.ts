export interface UserState{
    id: string | number;
    name: string;
    token?: string;
    ruls: Ruls;
    loading: LoadingStatus;
}

export interface User{
    id: string | number;
    name: string;
    ruls: Ruls;
    email: string;
    registratinData: Date;
    banTime: Date;
    status: boolean;
}

export enum Ruls{
    user = 0,
    admin = 1
}

export interface LoadingStatus{
        status: boolean;
        error?: boolean;
        message?: string;
}

export interface SignupStatus extends LoadingStatus{

}
