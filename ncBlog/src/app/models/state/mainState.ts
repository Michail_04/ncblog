import { UserState } from './userState';
import { Dialogs } from './dislogsState';
import { SectionsState } from './sections';
import { PostsState } from './posts';

export interface MainState {
    user: UserState;
    dialogs: Dialogs;
    sections: SectionsState,
    posts: PostsState
}