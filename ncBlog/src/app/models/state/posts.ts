import { Sections } from './sections';

export interface LoadOptions{

}

export interface Comments{
    _id: string;
    creator
    dateAdd: Date;
    dateUpd: Date;
    content: string;
}

export interface Posts {
    comments?: any[];
    showComment: boolean;
    visibility?: number;
    emailAcces?: [];
    sections?: Sections[];
    _id?: string;
    title?: string;
    content?: string[];
    creator?: Creater;
    dateAdd?: Date;
}

export interface Creater{
    _id: string;
    email: string;
    name: string;
    registratinData: Date;
}

export interface PostsBlock{
    data: Posts[];
    status: boolean;
    error: boolean;
    errorMsg?: string;
    page?: number;
    hasNext: boolean;
    hasPosts: boolean;
    hasPrev: boolean;
    totalPages: number;
    postsOnPage: number;
    totalPosts: number;
}

export interface PostsState{
    posts: PostsBlock;
}