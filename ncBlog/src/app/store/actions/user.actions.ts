import { Action } from '@ngrx/store';
import * as UsersType from 'src/app/models/state/userState';
import { LoginCredential } from 'src/app/models/response';

const USER_ACTIONS = "USER_ACTIONS";

export const USER_DATA_LOADING = "USER_DATA_LOADING__" + USER_ACTIONS;
export const USER_DATA_LOADING_COMPLITE = "USER_DATA_LOADING_COMPLITE__" + USER_ACTIONS;
export const USER_DATA_LOADING_ERROR = "USER_DATA_LOADING_ERROR__" + USER_ACTIONS;

export const USER_DATA_INIT_ERORR = "USER_DATA_INIT_ERORR__" + USER_ACTIONS;
export const INIT_STORE = "INIT_STORE" + USER_ACTIONS;
export const INIT_STORE_DATA = "INIT_STORE_DATA__" + USER_ACTIONS;

export const STOP_LOADING = "STOP_LOADING__" + USER_ACTIONS;

export const LOGOUT = "LOGOUT" + USER_ACTIONS;

/***************** LOGIN *********************/
export class UserLoading implements Action{
    readonly type = USER_DATA_LOADING;
    constructor(public data: LoginCredential) { }
}
export class UserLoaded implements Action{
    readonly type = USER_DATA_LOADING_COMPLITE;
    constructor(public data: UsersType.UserState) { }
}
export class UserLoadingError implements Action{
    readonly type = USER_DATA_LOADING_ERROR;
    constructor(public data: UsersType.LoadingStatus) { }
}

export class StopLoading implements Action{
    readonly type = STOP_LOADING;
}

export class Loguot implements Action{
    readonly type = LOGOUT;
}
/***************** INIT *********************/
export class UserInitError implements Action{
    readonly type = USER_DATA_INIT_ERORR;
    constructor(public data: any) { }
}
export class InitStore implements Action{
    readonly type = INIT_STORE;
}
export class InitStoreData implements Action{
    readonly type = INIT_STORE_DATA;
    constructor(public data: UsersType.UserState) { }
}

export type AuthAction = UserLoading | UserLoaded | UserLoadingError | UserInitError;