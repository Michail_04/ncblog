import { Action } from '@ngrx/store';
import * as Posts from 'src/app/models/state/posts';

const POSTS = "POSTS_ACTION"

export const INIT_POSTS = 'INIT_SECTIONS__'+POSTS;

export const START_LOAD_POSTS = 'START_LOAD_POSTS__'+POSTS;
export const COMPLITE_LOAD_POSTS = 'COMPLITE_LOAD_POSTS__'+POSTS;
export const ERROR_LOAD_POSTS = 'ERROR_LOAD_POSTS__'+POSTS;

export const UPDATE_POSTS = 'UPDATE_POSTS__'+POSTS;

export class StartLoadPosts implements Action{
    readonly type = START_LOAD_POSTS;
}
export class CompliteLoadPosts implements Action{
    readonly type = COMPLITE_LOAD_POSTS;
    constructor(public data: Posts.PostsBlock) { }
}
export class ErrorLoadPosts implements Action{
    readonly type = ERROR_LOAD_POSTS;
    constructor(public data: Posts.PostsBlock) { }
}

export class InitPosts implements Action{
    readonly type = INIT_POSTS;
    constructor(public data: Posts.PostsBlock) { }
}
export class UpdatePosts implements Action{
    readonly type = UPDATE_POSTS;
    constructor(public data: number) { }
}
