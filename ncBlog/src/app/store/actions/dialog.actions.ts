import { Action } from '@ngrx/store';

const DIALOG_ACTIONS = "DIALOG_ACTIONS";

export const TOGGLE_AUTH_DIALOG = "TOGGLE_DIALOG_OPEN" + DIALOG_ACTIONS;
export const TOGGLE_SIGNIN_DIALOG = "TOGGLE_SIGNIN_DIALOG" + DIALOG_ACTIONS;
export const TOGGLE_RESET_PASS_DIALOG = "TOGGLE_RESET_PASS_DIALOG" + DIALOG_ACTIONS;

export const TOGGLE_TOKEN_DIALOG = "TOGGLE_TOKEN_DIALOG" + DIALOG_ACTIONS;

export class ToggleAuthDialog implements Action{
    readonly type = TOGGLE_AUTH_DIALOG;
    constructor(public data: boolean) { }
}
export class ToggleSignInDealog implements Action{
    readonly type = TOGGLE_SIGNIN_DIALOG;
    constructor(public data: boolean) { }
}
export class ToggleResetPassDealog implements Action{
    readonly type = TOGGLE_RESET_PASS_DIALOG;
    constructor(public data: boolean) { }
}

export class ToggleTokenDialog implements Action{
    readonly type = TOGGLE_TOKEN_DIALOG;
    constructor(public data: boolean) { }
}

export type DialogAction = 
ToggleAuthDialog          |           
ToggleSignInDealog        |
ToggleResetPassDealog      
