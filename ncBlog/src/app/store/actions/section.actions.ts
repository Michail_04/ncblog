import { Action } from '@ngrx/store';
import * as Sections from 'src/app/models/state/sections';


const SECTIONS = "SECTION_ACTION"

export const INIT_SECTIONS = 'INIT_SECTIONS__'+SECTIONS;

export const START_LOAD_SECTIONS = 'START_LOAD_SECTIONS__'+SECTIONS;
export const COMPLITE_LOAD_SECTIONS = 'COMPLITE_LOAD_SECTIONS__'+SECTIONS;
export const ERROR_LOAD_SECTIONS = 'ERROR_LOAD_SECTIONS__'+SECTIONS;


export class StartLoadSections implements Action{
    readonly type = START_LOAD_SECTIONS;
}
export class CompliteLoadSections implements Action{
    readonly type = COMPLITE_LOAD_SECTIONS;
    constructor(public data: Sections.SectionsBlock) { }
}
export class ErrorLoadSections implements Action{
    readonly type = ERROR_LOAD_SECTIONS;
    constructor(public data: Sections.SectionsBlock) { }
}


export class InitSections implements Action{
    readonly type = INIT_SECTIONS;
    constructor(public data: Sections.SectionsBlock) { }
}