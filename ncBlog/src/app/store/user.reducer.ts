import { UserState, Ruls, LoadingStatus } from '../models/state/userState';
import * as UserActions from './actions/user.actions';
import { environment } from 'src/environments/environment';


function userDataLoaded(state: UserState, data:UserState){
    return Object.assign({}, state, data);
}
function userDataLoadingError(state:UserState, data:LoadingStatus){
    return Object.assign({}, state, {loading:{...data}});
}
function userDataLoading(state:UserState){
    return Object.assign({}, state, {loading:{status:true}});
}

function userInitErorr(state:UserState,data:boolean){
    return Object.assign({}, state, {loading:{error:false, message: '',status:false}});
}
function userInitData(state:UserState,data:UserState){
    return Object.assign({}, state, {...data});
}

function stopLoading(state:UserState){
    return Object.assign({}, state, { loading:{...state.loading, status: false} });
}
function startLoading(state:UserState){
    return Object.assign({}, state, { loading: {...state.loading, status: true} });
}

function logout(state:UserState){
    return Object.assign({}, state, initialState);
}

const initialState:UserState = 
{
    id: '',
    name: '',
    token: '',
    ruls: Ruls.user,
    loading: {
        message: '',
        error: false,
        status: false
    }
}

export function userReduser(state = initialState, action: UserActions.AuthAction){
    if(!environment.production)console.log('userReduser',action.type)
    switch(action.type) {
        case UserActions.INIT_STORE:
            return startLoading(state);
        case UserActions.STOP_LOADING:
            return stopLoading(state);
        
        case UserActions.INIT_STORE_DATA:
            return userInitData(state, action.data as UserState);
        case UserActions.USER_DATA_INIT_ERORR:
            return userInitErorr(state, action.data as boolean);


        case UserActions.USER_DATA_LOADING:
            return userDataLoading(state);
        case UserActions.USER_DATA_LOADING_COMPLITE:
            return userDataLoaded(state, action.data as UserState);
        case UserActions.USER_DATA_LOADING_ERROR:
            return userDataLoadingError(state, action.data as LoadingStatus);

        case UserActions.LOGOUT:
            return logout(state);

        default:
            return state;
    }
}
