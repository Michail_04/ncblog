import { environment } from 'src/environments/environment';
import * as Posts  from '../models/state/posts';
import * as PostsActions  from './actions/posts.actions';

function startLoading(state: Posts.PostsState){
    return Object.assign({}, state, {posts: {...state.posts, status:true}});
}
function compliteLoading(state: Posts.PostsState, actions: Posts.PostsBlock){
    return Object.assign({}, state, {posts:{...state.posts, ...actions}});
}
function errorLoading(state: Posts.PostsState,  actions: Posts.PostsBlock){
    return Object.assign({}, state, {posts:{...state.posts, ...actions}});
}

const initPostsBlock: Posts.PostsBlock = {
    data: [],
    status: false,
    error: false,
    errorMsg: null,
    page: null,
    hasNext: null,
    hasPosts: null,
    hasPrev: null,
    totalPages: null,
    postsOnPage: null,
    totalPosts: null
}

const initialState: Posts.PostsState ={
    posts: initPostsBlock
}

export function postsReduser(state = initialState, action){
    if(!environment.production)console.log('postsReduser',action.type)
    switch(action.type) {
        case PostsActions.START_LOAD_POSTS:
            return startLoading(state);
        case PostsActions.COMPLITE_LOAD_POSTS:
            return compliteLoading(state, action.data);
        case PostsActions.ERROR_LOAD_POSTS:
            return errorLoading(state, action.data);    
        default:
            return state;
    }
}