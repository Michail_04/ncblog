import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import * as UserActions from './../actions/user.actions';
import * as DialogActions from './../actions/dialog.actions';
import * as PostsActions from './../actions/posts.actions'    
import { map, switchMap, catchError, mergeMap, tap } from 'rxjs/operators';
import { AuthService } from 'src/app/servises/auth.service';
import { LoginErrorResponse, LoginSuccesResponse, LoginCredential, checkTokenResp } from 'src/app/models/response';
import { of } from 'rxjs';
import { SectionsService } from 'src/app/servises/sections.service';

@Injectable()
export class AuthEffects {

  private debb:boolean = false;

  constructor(
    private actions$: Actions,
    private auth: AuthService,
    private sections: SectionsService
  ) {}

  @Effect()
  initData$ = this.actions$.pipe(
    ofType(UserActions.INIT_STORE),
    switchMap(()=>{
        return this.auth.verifyToken().pipe(
            map((resp: checkTokenResp) =>{
                if(this.debb) console.log(resp);
                return new UserActions.InitStoreData({...resp, loading:{error:false,status:false}});
            }),
            catchError(error => {
                return of(new UserActions.StopLoading());
            })
        );
    })
  );
  @Effect()
  logoutAction$ = this.actions$.pipe(
    ofType(UserActions.LOGOUT),
    map(()=>{
        localStorage.removeItem('token');
        return new PostsActions.UpdatePosts(1)
    })
  );
  
  @Effect()
  userState$ = this.actions$.pipe(
    ofType(UserActions.USER_DATA_LOADING),
    map((action: UserActions.UserLoading) => {
       if(this.debb)console.log(action);
       return action.data;
    }),
    switchMap((authData: LoginCredential)=>{
        if(this.debb) console.log(authData);
        return this.auth.login(authData).pipe(
            mergeMap((user: LoginSuccesResponse) => {
                if(this.debb) console.log(user);
                this.auth.setToken(user.token);
                this.sections.getNotAddedSections();
                return[
                    {
                        type: UserActions.USER_DATA_LOADING_COMPLITE,
                        data: {...user,loading:{error:false,status:false}}
                    },
                    {
                        type: DialogActions.TOGGLE_AUTH_DIALOG,
                        data: false
                    },
                    {
                        type: PostsActions.UPDATE_POSTS,
                        data: 1
                    }
                ];
            }),
            catchError(err => {
                if(this.debb)console.log(err);
                const errorData: LoginErrorResponse = err.status == 0 ? {message:'Ошибка на сервере'} : err.error;
                return of( new UserActions.UserLoadingError({ status:false, message: errorData.message, error: true}));
            })
        )
    })
  )
}