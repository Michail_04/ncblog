import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { PostsService } from 'src/app/servises/posts.service';
import * as PostsActions from '../actions/posts.actions';
import { switchMap, map, catchError, mergeMap } from 'rxjs/operators';
import { PostsResponse } from 'src/app/models/response';
import { of } from 'rxjs';


@Injectable()
export class PostsEffects {

  private debb:boolean = false;

  constructor(
    private actions$: Actions,
    private posts: PostsService
  ) {}
  @Effect()
  updateData$ = this.actions$
  .pipe(
    ofType(PostsActions.UPDATE_POSTS),
    map((action: PostsActions.UpdatePosts) => {
        if(this.debb)console.log(action);
        return action.data;
     }),
     switchMap(data =>{
        return this.posts.getPosts(data).pipe(
            map((resp: PostsResponse) => {
                if(this.debb) console.log(resp);
                return new PostsActions.CompliteLoadPosts({
                    data: resp.posts,
                    status: false,
                    error: false,
                    hasNext: resp.hasNext,
                    hasPosts: resp.hasPosts,
                    hasPrev: resp.hasPrev,
                    page: resp.page,
                    totalPages: resp.totalPages,
                    postsOnPage: resp.postsOnPage,
                    totalPosts: resp.totalPosts
            })}),
            catchError(error => {
                if(this.debb)console.log(error);
                const errorMessage = error.status ? error.error.message : 'Ошибка сервера';
                const err = error.error.error ? error.error.error : true;
                return of(
                    new PostsActions.ErrorLoadPosts({
                        data: [],
                        status: false,
                        error: err,
                        errorMsg: errorMessage,
                        hasNext: null,
                        hasPosts: null,
                        page: 1,
                        totalPages: null,
                        hasPrev: false,
                        postsOnPage: null,
                        totalPosts: null
                    }));
            })
        )})
  );
    
  @Effect()
  initData$ = this.actions$
  .pipe(
    ofType(PostsActions.START_LOAD_POSTS),
    switchMap(()=>{
        return this.posts.getPosts()
        .pipe(
            map((resp: PostsResponse) =>{
                if(this.debb) console.log(resp);
                return new PostsActions.CompliteLoadPosts({
                    data: resp.posts,
                    status: false,
                    error: false,
                    hasNext: resp.hasNext,
                    hasPosts: resp.hasPosts,
                    page: resp.page,
                    totalPages: resp.totalPages,
                    hasPrev: resp.hasPrev,
                    postsOnPage: resp.postsOnPage,
                    totalPosts: resp.totalPosts,
                })
            }),
            catchError(error => {
                if(this.debb)console.log(error);
                const errorMessage = error.status ? error.error.message : 'Ошибка сервера';
                const err = error.error.error ? error.error.error : true;
                return of(
                    new PostsActions.ErrorLoadPosts({
                        data: [],
                        status: false,
                        error: err,
                        errorMsg: errorMessage,
                        hasNext: null,
                        hasPosts: null,
                        page: 1,
                        totalPages: null,
                        hasPrev: false,
                        postsOnPage: null,
                        totalPosts: null
                    }));
            }));
    }));
}