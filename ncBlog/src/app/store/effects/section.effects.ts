import { Actions, ofType, Effect } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { SectionsService } from 'src/app/servises/sections.service';
import * as SectionActions from './../actions/section.actions'
import { switchMap, map, catchError } from 'rxjs/operators';
import { SectionsResp } from 'src/app/models/response';
import { of } from 'rxjs';

@Injectable()
export class SectionEffects {

  private debb:boolean = false;

  constructor(
    private actions$: Actions,
    private section: SectionsService
  ) {}

  @Effect()
  initData$ = this.actions$
  .pipe(
    ofType(SectionActions.START_LOAD_SECTIONS),
    switchMap(()=>{
        return this.section.getSections()
        .pipe(
            map((resp: SectionsResp[]) =>{
                if(this.debb) console.log(resp);
                return new SectionActions.CompliteLoadSections({
                    data: resp.map(data => ({ ...data,id:data._id})),
                    status: false,
                    error: false
                })
            }),
            catchError(error => {
                if(this.debb)console.log(error);
                const errorMessage = error.status ? error.error.message : 'Ошибка сервера';
                const err = error.error.error ? error.error.error : true;
                return of(
                    new SectionActions.ErrorLoadSections({
                        data: [],
                        status: false,
                        error: err,
                        errorMsg: errorMessage
                    }));
            }));
    }));
}