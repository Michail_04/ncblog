
import { Dialogs } from '../models/state/dislogsState';
import * as Actions from './actions/dialog.actions';
import { environment } from 'src/environments/environment';
import { DialogService } from '../servises/dialog.service';
import { DialogType } from '../models/dialogs';
import { RegistrationComponent } from '../components/auth/registration/registration.component';
import { LoginComponent } from '../components/auth/login/login.component';
import { TokenDialogComponent } from '../components/auth/token-dialog/token-dialog.component';

function togleAuthDialog(state: Dialogs, data:boolean){
    !data ? DialogService.dialogDispose(DialogType.AuthDialog) : DialogService.openLoginDialog(LoginComponent);
    return Object.assign({}, state,{authDialog:data});
}
function togleSignInDialog(state: Dialogs, data:boolean){
    !data ? DialogService.dialogDispose(DialogType.SignUpDialog) : DialogService.openSigninDialog(RegistrationComponent);
    return Object.assign({}, state, {signinDialog:data});
}
function togleResetPassDialog(state: Dialogs, data:boolean){
    if(!data) DialogService.dialogDispose(DialogType.ResetPassDialog);
    return Object.assign({}, state, {resetPass:data});
}
function togleTokenDialog(state: Dialogs, data:boolean){
    !data ? DialogService.dialogDispose(DialogType.TokenDialog) : DialogService.openSigninDialog(TokenDialogComponent);
    return Object.assign({}, state, {resetPass:data});
}

const initialState:Dialogs = 
{
    authDialog: false,
    signinDialog: false,
    resetPass:false
}

export function dialogsReduser(state = initialState, action: Actions.DialogAction){
   if(!environment.production)console.log('dialogsReduser',action.type)
    switch(action.type) {
        case Actions.TOGGLE_TOKEN_DIALOG:
            return togleTokenDialog(state, action.data);
        case Actions.TOGGLE_AUTH_DIALOG:
            return togleAuthDialog(state, action.data);
        case Actions.TOGGLE_SIGNIN_DIALOG:
            return togleSignInDialog(state, action.data);
        case Actions.TOGGLE_RESET_PASS_DIALOG:
            return togleResetPassDialog(state, action.data);
        default:
            return state;
    }
}
