import { userReduser } from './user.reducer';
import { dialogsReduser } from './dialog.reducer';
import { ActionReducerMap } from '@ngrx/store';
import { MainState } from '../models/state/mainState';
import { sectionsReduser } from './sections.reduser';
import { postsReduser} from './postsReduser';

export const rootReducer: ActionReducerMap<MainState> = {
    user: userReduser,
    dialogs: dialogsReduser,
    sections: sectionsReduser,
    posts: postsReduser
};

