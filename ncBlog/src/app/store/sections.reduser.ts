import { environment } from 'src/environments/environment';
import * as Sections from '../models/state/sections';
import * as SectionsActions from '../store/actions/section.actions';

function startLoading(state: Sections.SectionsState){
    return Object.assign({}, state, {sections: {...state.sections,status:true}});
}
function compliteLoading(state: Sections.SectionsState, actions: Sections.SectionsBlock){
    return Object.assign({}, state, {sections:actions});
}
function errorLoading(state: Sections.SectionsState,  actions: Sections.SectionsBlock){
    return Object.assign({}, state, {sections: actions});
}

const initSectionBlock: Sections.SectionsBlock ={
    data: [],
    status: false,
    error: false,
    errorMsg: null
}

const initialState: Sections.SectionsState = 
{
    sections: initSectionBlock
}

export function sectionsReduser(state = initialState, action){
    if(!environment.production)console.log('sectionsReduser',action.type)
    switch(action.type) {
        case SectionsActions.START_LOAD_SECTIONS:
            return startLoading(state);
        case SectionsActions.COMPLITE_LOAD_SECTIONS:
            return compliteLoading(state, action.data as Sections.SectionsBlock);
        case SectionsActions.ERROR_LOAD_SECTIONS:
            return errorLoading(state, action.data as Sections.SectionsBlock);    
        default:
            return state;
    }
}