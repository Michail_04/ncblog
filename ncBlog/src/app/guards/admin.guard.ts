import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Observable, of} from "rxjs";
import { MainState } from '../models/state/mainState';
import { Store } from '@ngrx/store';
import { UserState } from '../models/state/userState';
import { Injectable } from '@angular/core';
import { switchMap, take } from 'rxjs/operators';

@Injectable()
export class AdminGuard implements CanActivate{


    constructor(private store: Store<MainState>){}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Observable<boolean> | boolean{
        return this.store.select('user').pipe(
            take(1),
            switchMap((userState:UserState) => {
                if(userState.ruls >=1) {
                    return of(true)
                } else {
                    return of(false)
                }
            })
        );
    }
}