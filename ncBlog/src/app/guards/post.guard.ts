import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Observable, of, forkJoin, merge} from "rxjs";
import { MainState } from '../models/state/mainState';
import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { PostsService } from '../servises/posts.service';
import { map, mergeMap } from 'rxjs/operators';

@Injectable()
export class PostGuard implements CanActivate{

    constructor(
        private store: Store<MainState>,
        private postServices: PostsService
        ){}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Observable<boolean> | boolean{
        return this.store.select('user').pipe(
            mergeMap(user =>{
                return this.postServices.getPost(route.params.id).pipe(
                    map(post =>{
                        if(user.ruls >=1 || !post['newSection']){
                            return true;
                        } else {
                            return false;
                        }
                    })
                )
            })
        );
    }
}