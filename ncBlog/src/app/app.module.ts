import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthService } from './servises/auth.service';
import { ComponentsModule } from './components/components.module';
import { ValidationService } from './servises/validation.service';
import { DialogService } from './servises/dialog.service';
import { OverlayModule } from '@angular/cdk/overlay';
import { APP_CONFIG } from './helpers/app.config';
import { StoreModule } from '@ngrx/store';
import { rootReducer } from './store/rootReducer';
import { TokenInterceptor } from './Interceptors/auth.intercepter';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from './store/effects/auth.effects';
import { SectionEffects } from './store/effects/section.effects';
import { WindowRef } from './helpers/window';
import { PostsService } from './servises/posts.service';
import { PostsEffects } from './store/effects/posts.effects';
import { environment } from 'src/environments/environment';
import { RouterModule } from '@angular/router';
import { AdminGuard } from './guards/admin.guard';
import { PostGuard } from './guards/post.guard';


const endpoint = !environment.production ? 'http://localhost:8080' : 'https://boiling-inlet-52404.herokuapp.com';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    ComponentsModule,
    HttpClientModule,
    StoreModule.forRoot(rootReducer),
    EffectsModule.forRoot([
      AuthEffects,
      SectionEffects,
      PostsEffects,
    ])
  ],
  providers: [
    PostGuard,
    AdminGuard,
    PostsService,
    AuthService,
    ValidationService,
    DialogService,
    WindowRef,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: APP_CONFIG, useValue: {apiEndpoint: endpoint} },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
