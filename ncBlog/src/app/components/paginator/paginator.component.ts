import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Store } from '@ngrx/store';
import { MainState } from 'src/app/models/state/mainState';
import { PostsService } from 'src/app/servises/posts.service';
import { Subject } from 'rxjs';
import { PostsBlock } from 'src/app/models/state/posts';
import { map } from 'rxjs/operators';

interface Paginator{
  id?: any;
  totalPages: number;
  hasPosts: boolean;
  page: number;
  hasNext: boolean;
  pages: number;
  hasPrev: boolean;
  postsOnPage: number;
  totalPosts: number;
}

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.less']
})
export class PaginatorComponent implements OnInit, OnChanges{

  @Input() data: Paginator;
  @Input() dispatch: any;
  @Input() paginatorType: string;
  @Input() emiter: Subject<{posts: PostsBlock, id: any}>;

  public show = false;
  constructor(
    private store: Store<MainState>,
    private postsService: PostsService
    ) { }

  ngOnInit() {}

  ngOnChanges(simpleChange: SimpleChanges){
    this.show = true;
  }

  next(){
    switch(this.paginatorType){
      case '':
      if (this.data.hasNext) {
        this.store.dispatch(new this.dispatch(+this.data.page + 1));
      }
      break;
      case 'section':
      if (this.data.hasNext) {
        this.postsService.getSectionPosts(this.data.id, +this.data.page + 1).pipe(
          map(data => this.emiter.next({posts: data, id: this.data.id}))
        ).subscribe()
      }
      break;
    }
  }

  prev(){
    switch(this.paginatorType){
      case '':
      if (this.data.hasPrev) {
        this.store.dispatch(new this.dispatch(+this.data.page - 1));
      }
      break;
      case 'section':
      if (this.data.hasPrev) {
        this.postsService.getSectionPosts(this.data.id, +this.data.page - 1).pipe(
          map(data => this.emiter.next({posts: data, id: this.data.id}))
        ).subscribe();
      }
      break;
    }
  }

}
