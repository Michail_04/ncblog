import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationService } from 'src/app/servises/validation.service';
import { Store } from '@ngrx/store';
import { MainState } from 'src/app/models/state/mainState';
import { DialogService } from 'src/app/servises/dialog.service';
import { AuthService } from 'src/app/servises/auth.service';
import { SignupStatus } from 'src/app/models/state/userState';
import * as DialogActioms from 'src/app/store/actions/dialog.actions';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.less']
})
export class RegistrationComponent {
  
  signinForm: FormGroup;
  nameError: boolean;
  emailError: boolean;
  passError: boolean;
  repassError: boolean;
  loading:SignupStatus  = {
    error: false,
    status: false,
    message: ''
  }

  constructor(
    private fg: FormBuilder,
    public validate: ValidationService,
    private store: Store<MainState>,
    private dialog: DialogService,
    private auth: AuthService
  ) {
    this.signinForm = this.fg.group({
      email: ['', Validators.compose([Validators.email, Validators.required]) ],
      name: ['', Validators.required],
      password: ['', Validators.compose([Validators.required,Validators.minLength(5)]) ],
      repassword: ['',Validators.compose([Validators.required, validate.hasMatch('password')]) ]
    })
   }

  signin(e: FormGroup){
    if (e.get('email').errors 
    && e.get('password').errors 
    && e.get('repassword').errors) {
      this.nameError = true;
      this.emailError = true;
      this.passError = true;
      this.repassError = true;
    }
    if (e.valid) {
      this.auth.signIn({
        name: e.value.name,
        email: e.value.email,
        password: e.value.password
      })
      .subscribe( res =>  {
        this.loading = res
        this.hideDialog(this.loading);
      });
    }
  }

  hideDialog(loading:SignupStatus){
    if(!loading.error && !loading.status){
      this.store.dispatch(new DialogActioms.ToggleSignInDealog(false));
      this.store.dispatch(new DialogActioms.ToggleAuthDialog(true));
    }
  }
  checkError(data){
    return Array.isArray(data) ? true : false;
  }
}
