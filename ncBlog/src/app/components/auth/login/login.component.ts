import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidationService } from 'src/app/servises/validation.service';
import { DialogService } from 'src/app/servises/dialog.service';
import { UserState } from 'src/app/models/state/userState';
import { Store } from '@ngrx/store';
import { MainState } from 'src/app/models/state/mainState';
import { Observable } from 'rxjs';
import * as Actions from './../../../store/actions/user.actions';
import * as DialogActions from './../../../store/actions/dialog.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent{

  loginForm: FormGroup;
  loginError: boolean;
  passError: boolean;
  loading: boolean = false;
  error: boolean;
  errorMessage: string = null;
  user$: Observable<UserState>;

  constructor(
    private fg: FormBuilder,
    public validate: ValidationService,
    private store: Store<MainState>,
    private dialog: DialogService
  ) {
    this.loginForm = this.fg.group({
      email: ['', Validators.compose([Validators.email, Validators.required]) ],
      password: ['', Validators.required]
    });
    this.user$ = this.store.select('user');
  }


  login(e: FormGroup) {
    
    if (this.loginForm.get('email').errors && this.loginForm.get('password').errors) {
      this.loginError = true;
      this.passError = true;
    }
    if (e.valid) {
      this.store.dispatch(new Actions.UserLoading({ email: e.value.email, password: e.value.password}));
    }
  }

  reparePassword(){
    // this.dialog.loginDialogRef = this.dialog.dialogDispose(this.dialog.loginDialogRef);
  }

  singin(){
    this.store.dispatch(new DialogActions.ToggleAuthDialog(false));
    this.store.dispatch(new DialogActions.ToggleSignInDealog(true));
  }

}
