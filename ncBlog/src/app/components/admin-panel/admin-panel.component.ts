import { Component, OnInit, OnDestroy } from '@angular/core';
import { MainState } from 'src/app/models/state/mainState';
import { Store } from '@ngrx/store';
import { ToggleSignInDealog } from 'src/app/store/actions/dialog.actions';
import { BehaviorSubject } from 'rxjs';
import { User } from 'src/app/models/state/userState';
import { Posts } from 'src/app/models/state/posts';
import { AuthService } from 'src/app/servises/auth.service';
import { UserService } from 'src/app/servises/user.service';
import { PostsService } from 'src/app/servises/posts.service';
import { SectionsService } from 'src/app/servises/sections.service';
import { Sections } from 'src/app/models/state/sections';
import { StartLoadSections } from 'src/app/store/actions/section.actions';
import { StartLoadPosts } from 'src/app/store/actions/posts.actions';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.less']
})
export class AdminPanelComponent implements OnInit, OnDestroy{


  public users$ = new BehaviorSubject<User[]>([]);

  public posts$ = new BehaviorSubject<Posts[]>([]);

  public comments$ = new BehaviorSubject<any[]>([]);

  public sections$ = new BehaviorSubject<Sections[]>([]);

  public users: User[] = [];
  public posts: Posts[] = [];
  public comments: any[] = [];

  constructor(
    private store: Store<MainState>,
    private sectionServices: SectionsService,
    private userService: UserService,
    private postsServices: PostsService
  ){}

  public sectionStatus = true;

  public showMsgCheck = false;
  public msg;
  ngOnInit() {
    this.sectionServices.getNotAddedSections().subscribe(data => this.sections$.next(data));
  }

  ngOnDestroy(){
    this.users$.unsubscribe();
    this.posts$.unsubscribe();
    this.comments$.unsubscribe();
    this.sections$.unsubscribe();
  }

  addUser(){
    this.store.dispatch(new ToggleSignInDealog(true))
  }

  findUser(search){
    this.userService.searchUser(search).subscribe(users =>{
      this.users$.next(users);
      this.users = users;
      this.posts$.next([]);
      this.comments$.next([]);
    })
  }

  showPosts(id){
    this.userService.showPosts(id).subscribe(posts  =>{
      this.posts$.next(posts);
      this.posts = posts;
    })
  }

  showComments(id){
    this.userService.showComments(id).subscribe(comments =>{
      this.comments$.next(comments);
      this.comments = comments;
    })
  }

  deletePost(postId,index){
    this.postsServices.deletePost(postId).subscribe(() =>{
      this.posts.splice(index,1)
      this.posts$.next( this.posts);
      this.showMsg('Удален')
    });
  }
  deleteComment(commentId,index){
    this.postsServices.deleteCommentbyId(commentId).subscribe(()=>{
      this.comments.splice(index,1);
      this.comments$.next( this.comments);
      this.showMsg('Удален')
    })
  }

  saveNewSection(value, elm: HTMLInputElement){
    this.sectionServices.saveSection(value,this.sectionStatus).subscribe(() =>{
      this.sectionServices.getNotAddedSections().subscribe(data => this.sections$.next(data));
      this.showMsg('Добавленно', elm);
    });
  }

  activate(id,status = true, ref: HTMLInputElement){
    this.sectionServices.updateSection(id,status).subscribe(() => {
      this.sectionServices.getNotAddedSections().subscribe(data => this.sections$.next(data));
      this.store.dispatch(new StartLoadSections());
      this.store.dispatch(new StartLoadPosts());
      this.showMsg('Добавленно');
    })
  }

  deleteUserData(id){
    this.userService.deleteUserData(id).subscribe(() =>{
      this.users.splice(this.users.findIndex(user => user['_id'] === id), 1);
      this.users$.next(this.users);
      this.showMsg('Удален');
    })
  }

  showMsg(msg, ElmRef = null){
    this.msg = '' + msg;
    this.showMsgCheck = true;
    setTimeout(()=>{
      if (ElmRef) ElmRef.value = '';
      this.showMsgCheck = false;
      this.msg = '';
    },2000)
  }
}
