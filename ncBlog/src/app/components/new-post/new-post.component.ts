import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { SectionsService } from 'src/app/servises/sections.service';
import { BehaviorSubject, Observable, fromEvent } from 'rxjs';
import { Sections } from 'src/app/models/state/sections';
import { MainState } from 'src/app/models/state/mainState';
import { Store } from '@ngrx/store';
import { UserState } from 'src/app/models/state/userState';
import { PostsService } from 'src/app/servises/posts.service';
import { Router } from '@angular/router';
import { StartLoadPosts } from 'src/app/store/actions/posts.actions';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.less']
})
export class NewPostComponent implements OnInit, AfterViewInit{

  @ViewChild('overlay') overlay: ElementRef;
  @ViewChild('sectionInput') sectionInput: ElementRef;
  @ViewChild('inputRuls') inputRuls: ElementRef;
  @ViewChild('rowsContainer') rowsContainer: ElementRef;
  @ViewChild('emailInput') emailInput: ElementRef;title
  @ViewChild('title') titleRef: ElementRef;
  
  public sections$ = new BehaviorSubject<Sections[]>([]);
  private sectionStore: Sections[];

  public saveData = {
    sectionId: null,
    showComments: true,
    ruls: 0,
    title: '',
    sections:[],
    email:[],
    newSection: false
  }
  public rows = [''];

  public ruls = {
    current: 0,
    types: [
      {title:'Все', value: 0, checked: false},
      {title:'Авторизованные', value: 1, checked: false},
      {title:'По email', value: 3, checked: false},
      {title:'Только я', value: 2, checked: false},
    ]
  }

  public sectionList = false;
  public rulsList = false;
  public addSectionDialog = false;
  public overlayShow = false;

  private user: UserState;

  private KeyCode = {
    shiftEnter: {13:13,8:8},
    downKey: {}
  }

  constructor(
    private sectionService: SectionsService,
    private store: Store<MainState>,
    private postServise: PostsService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.sectionService.getAllSections().subscribe(data =>{
      this.sections$.next(data);
      this.sectionStore = data;
    })
    this.store.select('user').subscribe(data => this.user = data);
  }

  ngAfterViewInit(){
    (this.overlay.nativeElement as HTMLElement)
    .addEventListener('click',(event: Event) =>{
      if(this.overlayShow){
          this.rulsList = false;
          this.sectionList = false;
          this.addSectionDialog = false;
          this.overlayShow = false;
      }
    });
  }

  changeInputSections(change: string){
    this.saveData.sectionId = '';
    this.rulsList = false;
    this.sectionList = true;
    if (change) {
      const section = this.sectionStore.filter(data => (data.title.toLowerCase().includes(change.toLowerCase())));
      this.sections$.next(section);
    } else {
      this.sections$.next(this.sectionStore);
    }
  }

  checkSection(section: Sections, elm: HTMLInputElement): void{
    elm.value = section.title;
    this.saveData.sectionId = section['_id'];
    const sections: Sections[] = this.sectionStore.map(val => ({...val}));
    sections.forEach(val => val.checked = val['_id'] === this.saveData.sectionId ? true : false);
    this.sections$.next(sections);
    this.sectionList = false;
    this.overlayShow = false;
  }

  showRulsList(){
    this.overlayShow = true;
    this.rulsList = true;
    this.sectionList = false;
    this.addSectionDialog = false;
  }

  checkRuls(type , elm: HTMLInputElement): void{
    elm.value = type.title;
    this.saveData.ruls = type.value;
    this.ruls.types.forEach(val => val.checked = val.value === type.value ? true : false);
    this.rulsList = false;
    this.overlayShow = false;
  }

  showSectionList(){
    this.overlayShow = true;
    this.rulsList = false;
    this.sectionList = true;
    this.addSectionDialog = false;
  }

  showAddDialog(inputNewSection:HTMLInputElement = null){
    if(inputNewSection && inputNewSection.value) {
      this.sectionService.saveSection(inputNewSection.value, false).subscribe(data =>{
        this.saveData.sectionId = data['_id'];
        this.sectionInput.nativeElement.value = data['title'];
        this.sectionStore.push(data as Sections);
        this.sections$.next( this.sectionStore);
        this.saveData.newSection = true;
      })
    }
    this.overlayShow = true;
    this.rulsList = false;
    this.sectionList = false;
    this.addSectionDialog = true;
  }

  newRow(event: KeyboardEvent, rowIndex){
    this.KeyCode.downKey[event.keyCode] = true;
    if (this.KeyCode.downKey[13] && this.KeyCode.downKey[16]) {
      return;
    } else if(event.keyCode === 13){
      event.preventDefault();
      this.rows.push('');
      setTimeout(() =>{
        this.rowsContainer.nativeElement.children[this.rows.length-1].focus();
      },0)
    } else if(event.keyCode === 8) {
      if (!this.rowsContainer.nativeElement.children[rowIndex].innerText.length) {
        this.rows.pop();
        setTimeout(()=>{
          this.rowsContainer.nativeElement.children[this.rows.length-1].focus();
        },0)
      }
    }
  }
  keyUp(event: KeyboardEvent){
    delete this.KeyCode.downKey[event.keyCode];
  }

  clearRow(index){
    if ( index === 0) {
      this.rows[index] = '';
      this.rowsContainer.nativeElement.children[index].focus();
    }
  }

  savePost(){
    const temp = this.prepare();
    if (temp) {
      const data = {
        sections: [this.saveData.sectionId],
        title: this.saveData.title,
        visibility: this.saveData.ruls,
        showComment: this.saveData.showComments,
        content: [].slice.call(this.rowsContainer.nativeElement.children,0)
        .map((data:HTMLElement) => data.innerText),
        creator: this.user.id,
        emailAcces: this.saveData.email,
        newSection: this.saveData.newSection
      }
      this.postServise.savePost(data).subscribe(data =>{
        this.store.dispatch(new StartLoadPosts())
        this.router.navigateByUrl('/post/' + data['_id']);
      });
    }
  }

  prepare(): boolean{
    this.inputRuls.nativeElement.style.borderBottomColor = '#4e799f';
    this.sectionInput.nativeElement.style.borderBottomColor = '#4e799f';
    this.titleRef.nativeElement.style.borderLeftColor = '#4e799f';
    if(!this.saveData.sectionId) {
      this.sectionInput.nativeElement.style.borderBottomColor = 'red';
      return false;
    }
    const section = this.sectionStore.filter(section => {
      return this.saveData.sectionId === section['_id']
    });
    if(!section.length){
      this.sectionInput.nativeElement.style.borderBottomColor = 'red';
      return false;
    }
    const ruls = this.ruls.types.filter(rul => (this.saveData.ruls === rul.value));
    if(!ruls.length) {
      this.inputRuls.nativeElement.style.borderBottomColor = 'red';
      return false;
    }
    if(!section.length){
      this.sectionInput.nativeElement.style.borderBottomColor = 'red';
      return false;
    }
    if(!this.user.id){
      return false
    }
    if(!this.saveData.title){
      this.titleRef.nativeElement.style.borderLeftColor = 'red';
      return false;
    }
    return true;
  }

  addEmail(){
    const email = this.emailInput.nativeElement.value;
    if (email) {
      this.saveData.email.push(email);
    } else {
      this.emailInput.nativeElement.style.borderBottomColor = 'red';
    }
  }
}
