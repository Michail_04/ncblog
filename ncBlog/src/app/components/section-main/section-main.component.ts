import { Component, OnInit } from '@angular/core';
import { SectionsService } from 'src/app/servises/sections.service';
import { Observable } from 'rxjs';
import { Sections } from 'src/app/models/state/sections';

@Component({
  selector: 'app-section-main',
  templateUrl: './section-main.component.html',
  styleUrls: ['./section-main.component.less']
})
export class SectionMainComponent implements OnInit {

  constructor(private sectionServis: SectionsService) { }

  public section$: Observable<Sections[]>;
  public notAddedSections$: Observable<Sections[]>;

  ngOnInit() {
    this.section$ = this.sectionServis.getAllSections();
    this.notAddedSections$ = this.sectionServis.getNotAddedSections();
  }

}
