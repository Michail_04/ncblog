import { Component, ViewChild, ViewChildren, QueryList, ElementRef, AfterViewInit, Renderer2 } from '@angular/core';
import { MainState } from 'src/app/models/state/mainState';
import { Store } from '@ngrx/store';
import * as DialogActions from 'src/app/store/actions/dialog.actions';
import * as UserActions from 'src/app/store/actions/user.actions';
import * as PostActions from 'src/app/store/actions/posts.actions';
import { Observable, BehaviorSubject } from 'rxjs';
import { UserState } from 'src/app/models/state/userState';
import { DialogService } from 'src/app/servises/dialog.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements AfterViewInit{
  
  public user$: Observable<UserState>
  public toggleMenu: boolean = false;
  public toggleEmiter = new BehaviorSubject<boolean>(false);
 
  @ViewChild('overlay') overlay: ElementRef;

  ngAfterViewInit(){
    this.render.listen(this.overlay.nativeElement,'click',()=> this.toggleMenu = false);
  }
  
  constructor
  (
    private store: Store<MainState>,
    private dialog: DialogService,
    private render: Renderer2
  ) 
  { 
    this.toggleEmiter.subscribe(data =>this.toggleMenu = data);
    this.user$ = this.store.select('user');
  }

  loginDialog(): void{
    this.store.dispatch(new DialogActions.ToggleAuthDialog(true))
  }

  signout(){
    this.store.dispatch(new UserActions.Loguot())
  }
}
