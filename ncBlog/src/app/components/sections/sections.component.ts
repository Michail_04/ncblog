import { Component, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, Output, Input, OnInit, OnChanges, SimpleChange, SimpleChanges} from '@angular/core';
import { MainState } from 'src/app/models/state/mainState';
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import * as Secions from 'src/app/models/state/sections';
import { SectionsService } from 'src/app/servises/sections.service';

@Component({
  selector: 'app-sections',
  templateUrl: './sections.component.html',
  styleUrls: ['./sections.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SectionsComponent implements OnInit{

  public sections$: Observable<Secions.SectionsState>;

  constructor
  (
    private store: Store<MainState>,
    private section: SectionsService
  ) 
  {
    this.sections$ = this.store.select('sections');
  }

  ngOnInit(){
      this.section.sectionId.subscribe(data => this.checkedSection(data));
  }

  trackByIdx(i) {
    return i;
  }

  checkedSection(id: any): void {
    this.sections$.forEach(data =>{
      data.sections.data.forEach(section =>{
        section.checked = section.id === id ? true : false;
      })
    })
  }
}
