import { Component, OnInit, ViewChild, AfterViewInit, ElementRef, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';
import * as Posts from '../../models/state/posts'
import { UpdatePosts } from '../../store/actions/posts.actions'
import { Store, select } from '@ngrx/store';
import { MainState } from 'src/app/models/state/mainState';
import { map, tap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { PostsService } from 'src/app/servises/posts.service';
import { SectionsService } from 'src/app/servises/sections.service';

@Component({
  selector: 'app-main-posts',
  templateUrl: './main-posts.component.html',
  styleUrls: ['./main-posts.component.less']
})
export class MainPostsComponent implements OnInit{

  public posts$: Observable<Posts.PostsBlock>
  public paginatorData;
  public update = UpdatePosts;
  public showSectionPosts = false;
  public paginatorType = '';
  public emiter = new Subject<{posts: Posts.PostsBlock, id: any}>();
  constructor(
    private store: Store<MainState>,
    private router: ActivatedRoute,
    private posts: PostsService,
    private section: SectionsService
    ){}
  
  setPaginator(data){
    this.paginatorData = {
      totalPages: data.totalPages || 1,
      hasPosts: data.hasPosts,
      page: data.page,
      hasNext: data.hasNext,
      hasPrev: data.hasPrev,
      postsOnPage: data.totalPosts ? data.postsOnPage : 0,
      totalPosts: data.totalPosts,
    }
  }
    
  ngOnInit(){
    this.emiter.subscribe((data: {posts: Posts.PostsBlock, id: any}) =>{
      this.posts$ = of(data.posts);
      this.setPaginator(data.posts);
      this.paginatorData.id = data.id;
    })

    this.router.params.subscribe(data =>{
      if (data.sectionid) {
        this.section.sectionId.next(data.sectionid)
        this.paginatorType = 'section';
        this.showSectionPosts = true;
        this.posts$ = this.posts.getSectionPosts(data.sectionid).pipe(
          tap(posts=> {
            this.setPaginator(posts);
            this.paginatorData.id = data.sectionid;
          })
        )
      }else{
        this.section.sectionId.next(data.sectionid)
        this.posts$ = this.store.pipe(
          select('posts'),
          map(data => data.posts),
          tap(data => this.setPaginator(data))
        );
      }
    })
  }
}