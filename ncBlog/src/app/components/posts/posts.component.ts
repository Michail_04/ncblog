import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostsService } from 'src/app/servises/posts.service';
import { Observable, of } from 'rxjs';
import {Posts} from '../../models/state/posts'
import { concat, map, concatAll, take } from 'rxjs/operators';
import { SectionsService } from 'src/app/servises/sections.service';
import { MainState } from 'src/app/models/state/mainState';
import { Store } from '@ngrx/store';
import { UserState } from 'src/app/models/state/userState';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.less']
})
export class PostsComponent implements OnInit {

  constructor(
    private router: ActivatedRoute,
    private posts: PostsService,
    private section: SectionsService,
    private store: Store<MainState>
  ) { }

  public post: Posts;
  public showPost = true;
  public user: UserState;
  public editComment = [];

  ngOnInit() {
    this.section.sectionId.next('');
    this.store.select('user').subscribe(user => this.user = user);
    this.router.params.subscribe(rout =>{
      if (rout.id) {
      this.posts.getPost(rout.id).subscribe(data => this.post = data);
      } else {
        this.showPost = false;
      }
    });
  }

  addComment(elm: HTMLTextAreaElement){
    if (elm.value) {
      this.posts.addComment(this.post, this.user, elm.value);
    } else {
      elm.style.borderColor = 'red';
    }
  }

  deleteComment(index){
    this.posts.deleteComment(index, this.post._id).subscribe(() =>{
      this.post.comments.splice(index,1);
    })
  }

  canDelete(index): boolean{
   return this.user.ruls >=1 ||this.user.id == this.post.comments[index].user._id
  }

  canEdit(index): boolean{
    return this.user.ruls >= 1 || this.user.id == this.post.comments[index].user._id
  }
  
  editCommet(index, elm: HTMLInputElement = null){
    if(elm){
      return this.posts.editComment(index, this.post._id, elm.value).subscribe(() =>{
        this.post.comments[index].content = elm.value;
        this.editComment[index] = false;
      });
    } else {
      this.editComment[index] = true;
      console.log( this.editComment[index] )
    }
   
  }
}
