import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './auth/login/login.component';
import { RegistrationComponent } from './auth/registration/registration.component';
import { TokenDialogComponent } from './auth/token-dialog/token-dialog.component';
import { PortalModule } from '@angular/cdk/portal';
import { OverlayModule } from '@angular/cdk/overlay';
import { SectionsComponent } from './sections/sections.component';
import { MainPostsComponent } from './main-posts/main-posts.component';
import { PaginatorComponent } from './paginator/paginator.component';
import { MatIconModule } from '@angular/material';
import { SectionMainComponent } from './section-main/section-main.component';
import { RouterModule } from '@angular/router';
import { PostsComponent } from './posts/posts.component';
import { NewPostComponent } from './new-post/new-post.component';
import { BrowserModule } from '@angular/platform-browser';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';

@NgModule({
  declarations: [
    HeaderComponent,
    MainComponent,
    LoginComponent,
    RegistrationComponent,
    TokenDialogComponent,
    SectionsComponent,
    MainPostsComponent,
    PaginatorComponent,
    SectionMainComponent,
    PostsComponent,
    NewPostComponent,
    AdminPanelComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    OverlayModule,
    PortalModule,
    MatIconModule
  ],
  exports: [
    HeaderComponent,
    MainComponent,
    LoginComponent,
    RegistrationComponent,
    TokenDialogComponent,
    SectionsComponent,
    MainPostsComponent,
    PaginatorComponent,
    SectionMainComponent,
    PostsComponent,
    NewPostComponent
  ],
  entryComponents: [
    LoginComponent,
    RegistrationComponent,
    TokenDialogComponent
  ]
})
export class ComponentsModule { }
