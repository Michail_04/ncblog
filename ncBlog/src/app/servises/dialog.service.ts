import { Injectable, ElementRef } from '@angular/core';
import { Overlay, OverlayConfig, OverlayRef, ConnectionPositionPair } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { Store } from '@ngrx/store';
import { MainState } from '../models/state/mainState';
import * as Actions from '../store/actions/dialog.actions';
import * as UserActions from '../store/actions/user.actions';
import { DialogType } from '../models/dialogs';
import { Dialogs } from '../models/state/dislogsState';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  public loginDialogRef: OverlayRef;
  public signinDialogRef: OverlayRef;
  public resetPassDialogRef: OverlayRef;
  public tokenDialogRef: OverlayRef;

  public dialogType: DialogType;
  public dialogs: Dialogs;
  static dialogDispose;

  static openLoginDialog;
  static openSigninDialog;
  static openTokenDialog;

  constructor(
    private overlay: Overlay,
    private store: Store<MainState>
    ) {
      DialogService.dialogDispose = this.dialogDispose.bind(this);
      DialogService.openLoginDialog = this.openLoginDialog.bind(this);
      DialogService.openSigninDialog = this.openSigninDialog.bind(this);
      DialogService.openTokenDialog = this.openSigninDialog.bind(this);
    }

  
  private getOverlayConfig(config = {}): OverlayConfig {

    const positionStrategy = this.overlay.position()
      .global()
      .centerHorizontally()
      .centerVertically(); 
      
    const DEFAULT_CONFIG = {
      hasBackdrop: true,
      backdropClass: 'dark-backdrop',
      panelClass: 'overlay1-pane',
      scrollStrategy: this.overlay.scrollStrategies.block(),
      positionStrategy
    }

    config = Object.keys(config).length ? config : DEFAULT_CONFIG;

    return new OverlayConfig({...config});
  }

  private getOverlayRef(config): OverlayRef{
    return  this.overlay.create(this.getOverlayConfig(config));
  }

  openLoginDialog(Component,config:OverlayConfig = {}): void{
    if(this.loginDialogRef) return;
    this.loginDialogRef = this.getOverlayRef(config);
    this.loginDialogRef.attach(new ComponentPortal(Component));
    this.store.dispatch(new Actions.ToggleAuthDialog(true))
    this.store.dispatch(new UserActions.UserInitError({}));
    this.loginDialogRef.backdropClick().subscribe(_ =>{
      this.store.dispatch(new Actions.ToggleAuthDialog(false))
    });
  }

  // openTokenDialog(Component,config:OverlayConfig = {}): void{
  //   if(this.tokenDialogRef) return;
  //   this.tokenDialogRef= this.getOverlayRef(config);
  //   this.tokenDialogRef.attach(new ComponentPortal(Component));
  //   this.store.dispatch(new Actions.ToggleTokenDialog(true))
  //   this.loginDialogRef.backdropClick().subscribe(_ =>{
  //     this.store.dispatch(new Actions.ToggleTokenDialog(false))
  //   });
  // }

  openSigninDialog(Component,config:OverlayConfig = {}): void{
    if(this.signinDialogRef) return;
    this.signinDialogRef = this.getOverlayRef(config);
    this.signinDialogRef.attach(new ComponentPortal(Component));
    this.store.dispatch(new Actions.ToggleSignInDealog(true))
    this.signinDialogRef.backdropClick().subscribe(_ =>{
      this.store.dispatch(new Actions.ToggleSignInDealog(false))
    });
  }

  dialogDispose(dialog: DialogType): void{
    switch(dialog){
      case DialogType.AuthDialog:
      if(this.loginDialogRef) {
        this.loginDialogRef.dispose();
        this.loginDialogRef = null;
      }
      break;
      case DialogType.TokenDialog:
      if(this.tokenDialogRef) {
        this.tokenDialogRef.dispose();
        this.tokenDialogRef = null;
      }
      break;
      // case DialogType.ResetPassDialog:
      // if(this.) {
      //   this.loginDialogRef.dispose();
      //   this.loginDialogRef = null;
      //   this.store.dispatch(new Actions.ToggleAuthDialog(false));
      // }
      // break;
      case DialogType.SignUpDialog:
      if(this.signinDialogRef) {
        this.signinDialogRef.dispose();
        this.signinDialogRef = null;
      }
      break;
    }
  }
}
