import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../helpers/app.config';
import { Observable, BehaviorSubject } from 'rxjs';
import { LoginCredential, SigninCredential } from '../models/response';
import { SignupStatus } from '../models/state/userState';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private config:AppConfig;
  private debb = false;

  constructor(
    private http: HttpClient,
    private ingector: Injector,
    ) {
      this.config = this.ingector.get(APP_CONFIG)
    }

    login(credential: LoginCredential): Observable<any>{
      return this.http.post(`${this.config.apiEndpoint}/auth/login`, credential);
    }

    signIn(credential: SigninCredential): Observable<SignupStatus>{
      const subject = new BehaviorSubject<SignupStatus>({error: false, status: true});
      this.http.put(`${this.config.apiEndpoint}/auth/signup`, credential)
      .subscribe(
        res =>{
          if(this.debb)console.log(res);
          subject.next({error: false, status: false});
        },
        err =>{
          if(this.debb)console.log(err);
          const message = err.status ?  err.error.message : 'Ошибка сервера';
          subject.next({error: true, status: false, message: message});
        }
      )
      return subject;
    }

    verifyToken(): Observable<any>{
      return this.http.get(`${this.config.apiEndpoint}/auth/verify`);
    }

    setToken(token:string):void{
      if(token.length) localStorage.setItem('token',token);
    }

    getToken(token:string){
      return localStorage.getItem(token);
    }

    deleteToken():void{
      if(this.checkToken('token')) localStorage.removeItem('token');
    }

    checkToken(token:string):boolean{
      return localStorage.getItem(token) ? true : false;
    }
}
