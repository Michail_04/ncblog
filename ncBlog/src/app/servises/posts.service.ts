import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../helpers/app.config';
import { Observable, of } from 'rxjs';
import { PostsBlock, Posts} from '../models/state/posts';
import { map, catchError } from 'rxjs/operators';
import { PostsResponse } from '../models/response';

@Injectable({
    providedIn: 'root'
})
export class PostsService {

    private config:AppConfig;
    private post: Observable<Posts>

    constructor(
        private http: HttpClient,
        private ingector: Injector,
    ){
        this.config = this.ingector.get(APP_CONFIG);
    }

    getPosts(page = 1){
        const httpParams = new HttpParams().set('pages',page+'');
        return this.http.get(`${this.config.apiEndpoint}/posts`,{
            params: httpParams
        });
    }

    getSectionPosts(id,page = 1): Observable<PostsBlock>{
        const httpParams = new HttpParams().set('pages',page+'');
        return this.http.get(`${this.config.apiEndpoint}/posts/section/`+id,{
            params: httpParams
        }).pipe(
            map((data: PostsResponse) => ({
                data: data.posts,
                status: false,
                error: false,
                hasNext: data.hasNext,
                hasPosts: data.hasPosts,
                hasPrev: data.hasPrev,
                page: data.page,
                totalPages: data.totalPages,
                postsOnPage: data.postsOnPage,
                totalPosts: data.totalPosts
            }))
        );
    }

    getPost(id): Observable<Posts>{
        return this.http.get(`${this.config.apiEndpoint}/posts/`+id) as Observable<Posts>;
    }

    addComment(post: Posts, user, comments): void{
        const data = {
            user: user.id,
            content: comments
        };
        const commentsArray = post.comments.slice(0);
        commentsArray.push(data);
        this.http.put(`${this.config.apiEndpoint}/posts/comment/` + post._id,{data: commentsArray}).subscribe(lastComment =>{
                console.log(lastComment);
                post.comments.push(lastComment);
                console.log( post.comments);
        })
    }

    deleteComment(index, id): Observable<boolean> {
       return this.http.delete(`${this.config.apiEndpoint}/posts/comment/${id}/${index}`) as Observable<boolean>;
    }

    editComment(index, id, value):  Observable<boolean>{
        return this.http.post(`${this.config.apiEndpoint}/posts/comment/${id}/${index}`,{data: value}) as Observable<boolean>;
    }

    savePost(data){
        return this.http.put(`${this.config.apiEndpoint}/posts/`,data);
    }

    deletePost(id){
        return this.http.delete(`${this.config.apiEndpoint}/posts/` + id);
    }

    deleteCommentbyId(id){
        return this.http.delete(`${this.config.apiEndpoint}/posts/commentbyid/${id}`) as Observable<boolean>;
    }
}