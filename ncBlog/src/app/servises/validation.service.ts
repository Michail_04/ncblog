import { Injectable } from '@angular/core';
import { FormControl, AbstractControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor() { }

  public validationsProperty = {
    name: [
      { type: 'required', message: 'Поле не может быть пустым' },
    ],
    email: [
      { type: 'required', message: 'Поле не может быть пустым' },
      { type: 'email', message: 'Некорректный адрес' }
    ],
    password: [
      { type: 'required', message: 'Поле не может быть пустым' },
      { type: 'minlength', message: 'Не допустимый пароль' },
    ],
    confirm_password: [
      { type: 'required', message: 'Поле не может быть пустым' },
      { type: 'mismatchedPassword', message: 'Пароль не совпадает' }
    ]
  };

  getValidationsProperty() {
    return this.validationsProperty;
  }

  hasMatch(prop) {
    return function(input: AbstractControl):{[key: string]: any} | null{
      if (!input.root || !input.root.get('password')) {
        return null;
      }
      const exactMatch = input.root.get(prop).value === input.value;
      return exactMatch ? null : { mismatchedPassword: true };
    }
  }
}
