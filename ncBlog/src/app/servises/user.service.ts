import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { AppConfig, APP_CONFIG } from '../helpers/app.config';
import { Store } from '@ngrx/store';
import { MainState } from '../models/state/mainState';
import { User } from '../models/state/userState';
import { debounceTime, map } from 'rxjs/operators';
import { Posts } from '../models/state/posts';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private config:AppConfig;

  constructor(
    private http: HttpClient,
    private ingector: Injector,
    private store: Store<MainState>
    
  ) { 
    this.config = this.ingector.get(APP_CONFIG)
  }

  searchUser(search): Observable<User[]>{
    return this.http.post(`${this.config.apiEndpoint}/users`, {search:search}) as Observable<User[]>;
  }

  showPosts(id){
    return this.http.get(`${this.config.apiEndpoint}/users/posts/` + id) as Observable<Posts[]>;
  }
  showComments(id){
    return this.http.get(`${this.config.apiEndpoint}/users/comments/` + id) as Observable<any[]>;
  }

  deleteUserData(id){
    return this.http.delete(`${this.config.apiEndpoint}/users/` + id)
  }
}
