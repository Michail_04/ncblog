import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../helpers/app.config';
import { Observable, of, BehaviorSubject, Subject } from 'rxjs';
import { Sections } from '../models/state/sections';
import { map, take, tap } from 'rxjs/operators';
import { MainState } from '../models/state/mainState';
import { Store } from '@ngrx/store';

@Injectable({
  providedIn: 'root'
})
export class SectionsService {

  private config:AppConfig;
  private allSections: Observable<Sections>;
  private userInfo: Observable<Sections>;

  public sectionId = new Subject<string>();

  constructor
  (
    private http: HttpClient,
    private ingector: Injector,
    private store: Store<MainState>
  ) 
  {
    this.config = this.ingector.get(APP_CONFIG)
  }
  
  getSections(){
    return this.http.get(`${this.config.apiEndpoint}/sections`);
  }
  getAllSections(): Observable<Sections[]>{
    return this.http.get(`${this.config.apiEndpoint}/sections/all`) as Observable<Sections[]>;
  }

  getNotAddedSections(): Observable<Sections[]>{
    const section = new BehaviorSubject<Sections[]>([]);
    this.store.select('user').subscribe(user => {
      if(user.ruls >= 1){
        this.http.get(`${this.config.apiEndpoint}/sections/notadded`).pipe(
          tap(data => section.next(data as Sections[]))
        ).subscribe();
      }
    });
    return section.asObservable();
  }

  saveSection(title, status){
    return this.http.put(`${this.config.apiEndpoint}/sections/`,{title, status})
  }

  updateSection(id,status): Observable<boolean>{
    return this.http.patch(`${this.config.apiEndpoint}/sections/`+id,{status}) as Observable<boolean>
  }

}
