import { Component } from '@angular/core';
import { DialogService } from './servises/dialog.service';
import { MainState } from './models/state/mainState';
import { Store } from '@ngrx/store';
import * as UserActions from './store/actions/user.actions';
import * as SecionsActions from 'src/app/store/actions/section.actions';
import * as PostsActions from 'src/app/store/actions/posts.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  constructor(
    private store: Store<MainState>
  ){
    this.initData();
  }

  initData(){
    this.store.dispatch(new UserActions.InitStore());
    this.store.dispatch(new SecionsActions.StartLoadSections());
    this.store.dispatch(new PostsActions.StartLoadPosts());
  }
}
