import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './components/main/main.component';
import { SectionMainComponent } from './components/section-main/section-main.component';
import { PostsComponent } from './components/posts/posts.component';
import { NewPostComponent } from './components/new-post/new-post.component';
import { AdminPanelComponent } from './components/admin-panel/admin-panel.component';
import { AdminGuard } from './guards/admin.guard';
import { PostGuard } from './guards/post.guard';

const routes: Routes = [
  { path: 'sections', component: SectionMainComponent },
  { path: 'sections/:sectionid', component: MainComponent },
  { path: 'post/:id', component: PostsComponent, canActivate: [PostGuard]},
  { path: 'new/post',  component: NewPostComponent},
  { path: 'adminlist',  component: AdminPanelComponent, canActivate: [AdminGuard]},
  { path: '', component: MainComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
