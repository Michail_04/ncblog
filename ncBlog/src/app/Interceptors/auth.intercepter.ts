import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Store } from '@ngrx/store';
import { MainState } from '../models/state/mainState';
import { switchMap, take } from 'rxjs/operators';
import { UserState } from '../models/state/userState';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public store: Store<MainState>) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.store.select('user').pipe(
        take(1),
        switchMap((userState:UserState) => {
        const localToken: string = localStorage.getItem('token');
        const token = localToken ? localToken : '';
        const clonereq = req.clone({headers: req.headers.set('Authorization', token)});
        if(!environment.production) console.log(clonereq);
            return next.handle(clonereq);
        }));
    }
}