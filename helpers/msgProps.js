exports.msg = {
    notAuthError: "Пользователь не авторизован",
    userUpdError: "Ошибка обновления",
    userDeleteError: "Ошибка удаления",
}
exports.status = {
    notAuth: 401,
    updError: 422,
    deleteError: 422,
    create: 201,
    createError: 422,
}