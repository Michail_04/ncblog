const rights = require('../helpers/ruls')


exports.checkOnAuth = checkOnAuth;
exports.filterPost = filterPost;
exports.canDeletePost = canDeletePost;
exports.canUpdPost = canUpdPost;
exports.getPostAccess = (post,req) => {
    return post.visibility < 1 || (checkOnAuth(post,req) && filterPost(post,req));
}

function filterPost(post,req){
    const status = 
    (post.visibility == 3 && post.emailAcces.includes(req.userEmail)) ||
    (post.visibility == 2 && post.creator.email == req.userEmail)     || 
    (post.visibility == 1);
    return status;
}

function checkOnAuth(post,req) {
    return post.visibility >= 1 && !req.auth.error;
}

function canDeletePost(req,post){
    return post.creator == req.userId || rights.ruls.deletePosts.includes(req.userRuls);
}

function canUpdPost(req,post){
    return post.creator == req.userId || rights.ruls.updPosts.includes(req.userRuls);
}

function canUpdStatus(req){
    return rights.ruls.updStatus.includes(req.userRuls);
}