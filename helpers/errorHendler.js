
exports.errorCreater = (error,message) =>{
    return {error,message};
}

exports.createError = (prop,status) =>{
    const error = new Error(prop);
    error.statusCode = status;
    error.message = prop;
    return error;
}