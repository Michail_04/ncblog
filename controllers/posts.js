const { validationResult } = require('express-validator/check');

const Post =  require('../models/posts');
const Section =  require('../models/sections');
const helpers = require('../helpers/checkHandlers')
const props = require('../helpers/msgProps')

const DEFAULT_COUNT_POSTS = 10;
const paginationCash = []

exports.getAllPosts = (req, res, next) =>{
    const pages = getCountPostsAndCurrentPage(req)
    const allPosts = [];

    allPosts.push(post({visibility: 0,newSection: false}));
    if(!req.auth.error){
        allPosts.push(
            post({visibility: 1, newSection: false}),
            post({visibility: 2, creator: req.userId, newSection: false}),
            post({visibility: 3, emailAcces: req.userEmail, newSection: false})
        );
    }
    Promise.all(allPosts).then(posts =>{
        posts = posts
        .reduce((a,b)=>a.concat(b),[])
        .sort((a,b) => Date.parse(a.dateAdd) > Date.parse(b.dateAdd) ? 1 : -1)
        res.json(paginations(req,posts,pages));
    });
}
exports.getPost = (req, res, next) =>{
    Post.posts.findOne({_id:req.params.id})
    .populate('creator', ['name','registratinData','email'])
    .populate('sections', ['title','dateAdd'])
    .populate('comments.user', ['name','registratinData','email'])
    .then(post =>{
        if(helpers.getPostAccess(post,req)){
            post.po
            res.status(200).json(post);
        }else{
            const error = new Error();
            error.message = 'Access denied';
            throw  error;
        }    
    })
    .catch(error => next(error));
}
exports.getPostsBySection = (req, res, next) => {
    const pages = getCountPostsAndCurrentPage(req);
    const allPosts = [];
    allPosts.push(post({visibility: 0, sections: req.params.id, newSection: false}));
    if(!req.auth.error){
        allPosts.push(
            post({visibility: 1, sections: req.params.id, newSection: false}),
            post({visibility: 2, sections: req.params.id, creator: req.userId, newSection: false}),
            post({visibility: 3, sections: req.params.id, emailAcces: req.userEmail, newSection: false})
        );
    }
    Promise.all(allPosts).then(posts =>{
        posts = posts
        .reduce((a,b)=>a.concat(b),[])
        .sort((a,b) => Date.parse(a.dateAdd) > Date.parse(b.dateAdd) ? 1 : -1)
        res.json(paginations(req,posts,pages));
    });
}
exports.createPost = (req, res, next) => {
    console.log(req.body.newSection)
    if(!req.auth.error){
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const error = new Error('Post created validation failed');
            error.statusCode = 422;
            error.data = errors.array();
            throw error;
        }
        new Post.posts({
            showComment: req.body.showComment,
            title: req.body.title,
            content: req.body.content,
            creator: req.body.creator,
            visibility: req.body.visibility,
            sections: req.body.sections,
            emailAcces: req.body.emailAcces,
            newSection: req.body.newSection
        })
        .save()
        .then(savePost =>{
            return Post.posts.findById(savePost._id)
            .populate('creator',['name','registratinData','email'])
            .populate('sections',['_id','title','dateAdd'])
        })
        .then(post => {
            return updSectionPosts(post);    
        })
        .then(post => {
            res.status(200).json(post)
        })
        .catch(() =>{
            next(createError(props.msg.userUpdError, props.status.updError));
        })
    }else{
        next(createError(props.msg.notAuth, props.status.notAuth));
    }
}
exports.updatePost = (req, res, next) => {
    if(!req.auth.error){
        Post.posts.findById(req.params.id)
        .then(post => {
            if(!post) throw Error;
            if(helpers.canUpdPost(req,post)){
                return Post.posts.findByIdAndUpdate(req.params.id,req.body.data)
            }
            throw Error;
        })
        .then(post =>{
            return Post.posts.findById(post._id)
            .populate('creator',['name','registratinData','email'])
            .populate('sections',['title','dateAdd'])
        })
        .then(post => res.status(200).json(post))
        .catch(err => {
            next(createError(props.msg.userUpdError, props.status.updError));
        })
    }else{
        next(createError(props.msg.notAuth, props.status.notAuth));
    }
}
exports.deletePost = (req, res, next) => {
    if(!req.auth.error){
        Post.posts.findById(req.params.id)
        .then(post => {
            if(!post) throw Error;
            if(helpers.canDeletePost(req,post)){
                return Post.posts.findByIdAndDelete(req.params.id)
            }
            throw Error;
        })
        .then(() => res.status(200).json({delete:true}))
        .catch(() => next(createError(props.msg.userDeleteError, props.status.deleteError)))
    }else{
        next(createError(props.msg.notAuth, props.status.notAuth));
    }
}

exports.addComment = (req, res, next) => {
    if(!req.auth.error){
        Post.posts.findById(req.params.id)
        .then(post => {
            if(!post) throw Error;
            return Post.posts.findByIdAndUpdate(req.params.id,{comments: req.body.data}, {new: true})
            .populate('comments.user', ['name','registratinData','email'])
        })
        .then(post =>{
            res.status(200).json(post.comments[post.comments.length-1]);
        })
        .catch(() => {
            next(createError(props.msg.userUpdError, props.status.updError));
        })
    }else{
        next(createError(props.msg.notAuth, props.status.notAuth));
    }
}

exports.deleteComment = (req, res, next) => {
    if(!req.auth.error){
        Post.posts.findById(req.params.id)
        .then(post => {
            if(!post) throw Error;
            post.comments.splice(req.params.index,1);
            post.set({comments:post.comments});
            return post.save();
        })
        .then(() =>{
            res.status(200).json(true);
        })
        .catch(() => {
            next(createError(props.msg.userUpdError, props.status.updError));
        })
    }else{
        next(createError(props.msg.notAuth, props.status.notAuth));
    }
}

exports.editComment = (req, res, next) => {
    if(!req.auth.error){
        Post.posts.findById(req.params.id)
        .then(post => {
            if(!post) throw Error;
            post.comments[req.params.index].content = req.body.data;
            return post.save();
        })
        .then(() =>{
            res.status(200).json(true);
        })
        .catch(() => {
            next(createError(props.msg.userUpdError, props.status.updError));
        })
    }else{
        next(createError(props.msg.notAuth, props.status.notAuth));
    }
}

exports.deleteCommentById = (req, res, next) => {
    if(!req.auth.error){
        Post.posts.find({'comments._id': req.params.id})
        .then(post => {
            post = post[0];
            if(!post) throw Error;
            const index = post.comments.findIndex(value => (value._id == req.params.id));
            post.comments.splice(index,1);
            post.set({comments:post.comments});
            return post.save();
        })
        .then(() =>{
            res.status(200).json(true);
        })
        .catch(error => {
            console.log(error)
            next(createError(props.msg.userUpdError, props.status.updError));
        })
    }else{
        next(createError(props.msg.notAuth, props.status.notAuth));
    }
}



function getCountPostsAndCurrentPage(req){
    return {
        countPosts: req.query.countPosts ? req.query.countPosts : DEFAULT_COUNT_POSTS,
        page: req.query.pages || 1
    }
}

function paginations(req,posts,pages){
    const currentPosts = posts.slice((pages.page-1) * pages.countPosts, pages.countPosts*pages.page);
    return {
        posts: currentPosts,
        totalPages: Math.ceil(posts.length / pages.countPosts),
        totalPosts: posts.length,
        postsOnPage: posts.length < pages.countPosts ?  posts.length : pages.countPosts,
        page: pages.page,
        hasPosts: currentPosts.length ? true : false,
        hasNext: pages.page * pages.countPosts < posts.length,
        hasPrev: pages.page > 1,
    }
}

function post(filter){
    return Post.posts.find(filter)
    .populate('creator',['name','registratinData','email'])
    .populate('sections',['title','dateAdd'])
}

function createError(prop,status){
    const error = new Error(prop);
    error.statusCode = status;
    error.message = prop;
    return error;
}

function updSectionPosts(post){
    return new Promise(resolve =>{
        Section.find({
            _id: {
                $in: post.sections.map(section => (section._id))
            }
        })
        .then(findSections =>{
            Promise.all(findSections.map(section =>{
                return Section.findOneAndUpdate(
                    {
                        _id: section._id
                    },
                    {
                        $push:{posts:post._id}
                    }
                )
            }))
            .then(() =>{
               resolve(post)
            });
        });
    });
}