const Sections =  require('../models/sections');
const Posts =  require('../models/posts');
const helpers =  require('../helpers/checkHandlers');
const { validationResult } = require('express-validator/check');
const helpersProps =  require('../helpers/msgProps');
const errorHeandlers =  require('../helpers/errorHendler');

exports.getSections = (req, res, next) => {
    Sections.find({status: true})
    .sort({posts:'desc'})
    .limit(10).select(['id','title','dateAdd','dateUpd'])
    .then(sections =>{
        res.status(200).json([...sections]);
    });
}

exports.getAllSections = (req, res, next) => {
    Sections.find({status: true})
    .sort({posts:'desc'})
    .select(['id','title','dateAdd','dateUpd','posts'])
    .then(sections =>{
        res.status(200).json([...sections]);
    });
}

exports.getNotAddedSections = (req, res, next) => {
    Sections.find({status: false})
    .select(['id','title','dateAdd','dateUpd','posts'])
    .then(sections =>{
        res.status(200).json([...sections]);
    });
}

exports.createSection = (req, res, next) => {
    if(!req.auth.error){
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const error = new Error('Section created validation failed');
            error.statusCode = 422;
            error.data = errors.array();
            throw error;
        }       
    new Sections({
        title: req.body.title,
        status: req.body.status
    })
    .save()
    .then(section => {
        res.status(helpersProps.status.create).json(section)
    })
    .catch(() => next(errorHeandlers.createError(props.msg.userUpdError, props.status.createError)))
    }else{
        next(errorHeandlers.createError(props.msg.notAuth, props.status.notAuth))
    }
}
exports.updateSection = (req, res, next) => {
    Sections.findById(req.params.id)
    .then(section => {
        section.set({status: req.body.status})
        return section.save();
    })
    .then(section => {
        return Posts.posts.find({'_id': {$in:  section.posts}})
    })
    .then(posts =>{
        const savePosts = [];
        posts.forEach(post =>{
            post.set({newSection:!req.body.status})
            savePosts.push(post.save())
        })
        return Promise.all(savePosts);
    })
    .then(status => res.status(200).json(true))
}
