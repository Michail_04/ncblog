const { validationResult } = require('express-validator/check');
const isAuth = require('../middleware/is-auth');

const User = require('../models/user');
const Posts = require('../models/posts');


exports.seahUser = (req, res, next) => {
    if(!req.auth.error){
        User.find({$or:[
            {email: {$regex: '.*' + req.body.search + '.*'}},
            {name: {$regex: '.*' + req.body.search + '.*'}}
        ]})
        .then(users =>{
            return res.status(200).json(users)
        })
        .catch( () => {
            const error = new Error('searchUser');
            error.statusCode = 411;
            error.message = 'searchUser';
            next(error);
        })
    } else {
        const error = new Error('searchUser');
        error.statusCode = 411;
        error.message = 'searchUser';
        next(error);
    }
}


exports.showPosts = (req, res, next) => {
    if(!req.auth.error){
        Posts.posts.find({creator: req.params.id})
        .then(users =>{
            return res.status(200).json(users)
        })
        .catch( () => {
            const error = new Error('searchUser');
            error.statusCode = 411;
            error.message = 'searchUser';
            next(error);
        });
    } else {
        const error = new Error('searchUser');
        error.statusCode = 411;
        error.message = 'searchUser';
        next(error);
    }
}


exports.showComments = (req, res, next) => {
    if(!req.auth.error){
        Posts.posts.find({'comments.user': req.params.id})
        .then(post =>{
            const comment = [];
            post.forEach(val => val.comments.forEach(commetVal =>{
                if (commetVal.user == req.params.id) {
                    comment.push({postId: val._id, commetVal});
                }
            }))
            return res.status(200).json(comment)
        })
        .catch( () => {
            const error = new Error('searchUser');
            error.statusCode = 411;
            error.message = 'searchUser';
            next(error);
        })
    } else {
        const error = new Error('searchUser');
        error.statusCode = 411;
        error.message = 'searchUser';
        next(error);
    }
}
exports.deleteData = (req, res, next) => {
    if(!req.auth.error){
        const id = req.params.id;
        Promise.all([
            User.findByIdAndDelete(id),
            Posts.posts.deleteMany({'creator': id}),
            Posts.posts.find({'comments.user': id})
            .then(posts =>{
                return new Promise(resolve =>{
                    const savePostStore = [];
                    posts.forEach(post => {
                        const comments = post.comments;
                        comments.forEach((comment, index) => {
                        if (comment._id === id) {
                            comments.splice(index, 1);
                        }
                    });
                    post.set({comments});
                    savePostStore.push(post.save());
                    });
                    Promise.all(savePostStore).then(()=>resolve())
                });
            })
        ])
        .then(() => res.status(200).json(true))
        .catch(() => res.status(411).json(false))
    } else {
        const error = new Error('searchUser');
        error.statusCode = 411;
        error.message = 'searchUser';
        next(error);
    }  
}
