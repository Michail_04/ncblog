const jwt = require('jsonwebtoken');
const error = require('../helpers/errorHendler');
const User = require('../models/user')

module.exports = (req, res, next) => {
  let decodedToken;
  const authHeader = req.get('Authorization');
  if (!authHeader){
    req.auth = error.errorCreater(true,'not auth header');
    next();
    return;
  }
  try {
    decodedToken = jwt.verify(authHeader, process.env.PRIVATE_KEY);
  } catch (err) {
    req.auth = error.errorCreater(true,'not decod token');
    next();
    return;
  }
  if (!decodedToken) {
    req.auth = error.errorCreater(true,'not decod token');
  }
  User.findById(decodedToken.userId)
  .then( user => {
    req.userEmail = user.email;
    req.userId = user._id;
    req.userRuls = user.ruls;
    req.auth = error.errorCreater(false);
    next();
  })
};

