
const express = require('express');
const app = express();

const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const routerApp = require('./routes/router');

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods',' GET, POST, PUT, DELETE, PATCH');
  res.setHeader('Access-Control-Allow-Headers', '*');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next(); 
});

app.use(express.static('./dist/ncBlog'));

app.use(routerApp);

app.use((error, req, res, next) => {
  const status = error.statusCode || 500;
  const messageArr = [];
  if(Array.isArray(error.data)){
    error.data.forEach(error => {
      messageArr.push({message: error.msg});
    });
  }
  const data = {
    message: messageArr.length ? messageArr : error.message,
    error: true
  }
  res.status(status).json(data);
});

app.get('/*',(req,res)=>{
  res.sendFile(path.join(__dirname,'/dist/your_app_name/index.html'));
})

mongoose
  .connect(
    `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASS}@ncblog-kr6b3.mongodb.net/${process.env.MONGO_TABLE_NAME}`, { useNewUrlParser: true}
  )
  .then(result => {
    app.listen(process.env.PORT || 8080);
  })
  .catch(err => console.log(err));