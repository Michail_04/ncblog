const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const sectionSchema = new Schema({
  id: Schema.Types.ObjectId,
  title: {
    type: String,
    required: true
  },
  status: {
    type: Boolean,
    default: false
  },
  dateAdd:{
      type: Date,
      default: new Date()
  },
  dateUpd:{
    type: Date,
    default: new Date()
  },
  posts: [{
      type: Schema.Types.ObjectId,
      ref: 'Posts',
  }]
      
});

module.exports = mongoose.model('Section', sectionSchema);
