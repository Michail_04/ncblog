const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  id: Schema.Types.ObjectId,
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  registratinData:{
    type: Date,
    default: Date.now
  },
  banTime:{
    type: Date
  },
  status: {
    type: Boolean,
    default: true
  },
  ruls:{
    type: Number,
    default: 0,
    required: true
  }
});

module.exports = mongoose.model('User', userSchema);
