const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const postSchema = new Schema(
  {
    id: Schema.Types.ObjectId,
    dateAdd:{
      type: Date,
      default: new Date()
    },
    dateUpd:{
      type: Date,
      default: new Date()
    },
    title: {
      type: String,
      required: true
    },
    content: {
      type: [String],
      required: true
    },
    creator: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true
    },
    showComment: {
      type: Boolean,
      default: true
    },
    comments:[
      {
        id: Schema.Types.ObjectId,
        user: {
          type: Schema.Types.ObjectId,
          ref: 'User',
          required: true
        },
        dateAdd:{
          type: Date,
          default: new Date()
        },
        dateUpd:{
          type: Date,
          default: new Date()
        },
        content:{
          type: String,
          required: true,
          default: []
        }
      }
    ], 
    visibility:{
      type: Number,
      default: 0,
      required: true
    },
    emailAcces:[{
      type: String,
      default: []
    }],
    sections:[{
        type: Schema.Types.ObjectId,
        ref: "Section",
        required: true
    }],
    newSection:{
      type: Boolean,
      required: true
    }
  },
);

module.exports = {
  posts: mongoose.model('Post', postSchema),
}