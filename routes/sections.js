const express = require('express');
const { body } = require('express-validator/check');
const sectionController = require('../controllers/sections');

const inAuth = require('../middleware/is-auth')

const router = express.Router();

router.get('', sectionController.getSections);

router.get('/all', sectionController.getAllSections);

router.get('/notadded', sectionController.getNotAddedSections);

router.put('', inAuth, 
[
    body('title')
    .trim()
    .not()
    .isEmpty()
    .withMessage('title не может быть пустым'),
]
, sectionController.createSection);

router.patch('/:id', inAuth, sectionController.updateSection);

// router.delete('', inAuth, sectionController.deleteSection);

module.exports = router;