const express = require('express');

const userController = require('../controllers/users');
const isAuth = require('../middleware/is-auth');

const router = express.Router();

router.get('/posts/:id', isAuth, userController.showPosts);

router.get('/comments/:id', isAuth, userController.showComments);

router.post('', isAuth, userController.seahUser);

router.delete('/:id', isAuth, userController.deleteData);

module.exports = router;
