const express = require('express');
const routerApp = express();
const auth = require('./auth');
const sections = require('./sections');
const posts = require('./posts');
const users = require('./users');


routerApp.use('/auth', auth);

routerApp.use('/sections', sections);

routerApp.use('/posts', posts);

routerApp.use('/users', users);

module.exports = routerApp;