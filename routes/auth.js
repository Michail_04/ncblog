const express = require('express');
const { body } = require('express-validator/check');

const authController = require('../controllers/auth');
const isAuth = require('../middleware/is-auth');
const User =  require('../models/user')
const router = express.Router();

router.put(
  '/signup',
  [
    body('email')
      .isEmail()
      .withMessage('Недопустимый email')
      .custom((value, { req }) => {
        return User.findOne({ email: value }).then(user => {
          if (user) {       
            return Promise.reject('Email занят');
          }
        });
      })
      .normalizeEmail(),
    body('password')
      .trim()
      .isLength({ min: 5 }),
    body('name')
      .trim()
      .not()
      .isEmpty()
  ],
  authController.signup
);

router.post('/login', authController.login);

router.get('/verify', isAuth, authController.verify);

module.exports = router;
