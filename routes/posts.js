const express = require('express');
const inAuth = require('../middleware/is-auth');
const { body } = require('express-validator/check');
const postsController = require('../controllers/posts');
const router = express.Router();



router.get('', inAuth, postsController.getAllPosts);

router.put('', inAuth,
[
    body('title')
        .trim()
        .not()
        .isEmpty()
        .withMessage('Заглавие не может быть пустым'),
    body('content')
        .trim()
        .not()
        .isEmpty()
        .withMessage('Контент не может быть пустым'),
    body('creator')
        .not()
        .isEmpty()
        .withMessage('Не указан создатель'),
    body('sections')
        .not()
        .isEmpty()
        .withMessage('Не указанна тема'),

],
postsController.createPost);

router.get('/:id', inAuth, postsController.getPost);

router.patch('/:id',inAuth, postsController.updatePost);

router.put('/comment/:id',inAuth, postsController.addComment);

router.delete('/comment/:id/:index',inAuth, postsController.deleteComment);

router.post('/comment/:id/:index',inAuth, postsController.editComment);

router.delete('/:id',inAuth, postsController.deletePost);

router.delete('/commentbyid/:id',inAuth, postsController.deleteCommentById);

router.get('/section/:id', inAuth, postsController.getPostsBySection);


module.exports = router;